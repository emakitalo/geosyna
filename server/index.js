const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http, {path: '/socket.io/'});
const glm = require('gl-matrix');

app.use(express.static('dist'));

const template = '../dist/index.html';

app.get('/', (req, res) => {
  res.sendFile(template);
});

const users = {};

io.use((socket, next) => {
  const data = socket.handshake.query;
  users[socket.id] =  {
    id: socket.id,
    longitude: parseFloat(data.longitude),
    latitude: parseFloat(data.latitude),
  };
  next();
});

io.on('connection', socket => {
  io.emit('user_joined', users);

  socket.on('unregister', id => {
    io.emit('user_left', id);
    delete users[id];
  });

  socket.on('data', data => {
    users[data.id] = data;
    io.emit('data', data);
  });

  socket.on('disconnect', () => {
    io.emit('user_left', socket.id);
    delete users[socket.id];
  });

  socket.on('terminate', function() {
    io.emit('user_left', socket.id);
    delete users[socket.id];
    socket.disconnect(0);
  });

  emitSignal();
});

function emitSignal() {
  io.emit('signal', Object.keys(users).length);
  setTimeout(emitSignal, 5000);
}

http.listen(8888);
