import '../scss/main.scss';
import 'ol/ol.css';
import {Map, View, Overlay} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import {transform} from 'ol/proj';
import io from 'socket.io-client';
import p5 from 'p5';
import 'p5/lib/addons/p5.sound.js';
import * as tone from 'tone';

Number.prototype.mapTo = function(in_min, in_max, out_min, out_max) {
  return ((this - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
};

const socket = io({autoConnect: false, path: '/socket.io/'});
let geoId = null;
const menu = createMenu();
let users = {};
let synths = {};

new p5();
masterVolume(0.5);

menu.infoVolumeSlider.setAttribute('data-value', 50);
menu.infoVolumeSlider.lastChild.style.transform =
  'translateX(' +
  (menu.infoVolumeSlider.firstChild.offsetWidth -
    menu.infoVolumeSlider.lastChild.offsetWidth) /
    2 +
  'px)';

menu.infoVolumeSlider.addEventListener(
  'mousemove',
  e => {
    setVolume(e);
    e.preventDefault();
  },
  false,
);

menu.infoVolumeSlider.addEventListener('touchmove', e => {
  setVolume(e);
});

function setVolume(e) {
  if (e.target.lastChild.getAttribute('active') === 'true') {
    const volume = parseFloat(e.target.getAttribute('data-value')).mapTo(
      1,
      e.target.firstChild.offsetWidth - e.target.lastChild.offsetWidth,
      0,
      1,
    );
    masterVolume(volume);
  }
}

let osMap = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
  ],
  view: new View({
    center: [0, 0],
    zoom: 0,
    minzoom: 6,
    maxzoom: 12,
  }),
});

function getData(data) {
  return {
    id: socket.id,
    longitude: data.coords.longitude,
    latitude: data.coords.latitude,
  };
}

function success(data) {
  if (!socket.connected) {
    socket.io.opts.query = getData(data);
    socket.open();
    menu.infoLocating.classList.remove('locating');
    menu.infoLocating.classList.add('located');
    menu.infoLocating.innerHTML = 'Located : ';
  } else {
    socket.emit('data', getData(data));
  }
}

function error() {
  alert('geolocation failed!');
  menu.infoLocating.classList.remove('located');
  socket.disconnect();
  menu.infoLocateBtn.classList.remove('disabled');
}

function addNewElement(t, e, c = '') {
  const element = document.createElement(e);
  element.innerHTML = t;
  element.classList.add(c);
  return element;
}

function addSlider(id) {
  const container = addNewElement('', 'DIV', 'sliderContainer');
  const pole = addNewElement('', 'DIV', 'sliderPole');
  const knob = addNewElement('', 'DIV', 'sliderKnob');

  container.setAttribute('data-name', id);
  knob.setAttribute('active', 'false');

  const startDrag = (posX, pole, knob) => {
    knob.setAttribute('active', 'true');
    const x = posX - pole.getBoundingClientRect().left;
    if (x < pole.offsetWidth - knob.offsetWidth && x > 0) {
      container.setAttribute(
        'data-value',
        parseInt(x.mapTo(1, pole.offsetWidth - knob.offsetWidth - 1, 0, 100)),
      );
      knob.style.transform = 'translateX(' + x + 'px)';
    }
  };

  const stopDrag = knob => {
    knob.setAttribute('active', 'false');
  };

  const moveKnob = (posX, pole, knob) => {
    const x = posX - pole.getBoundingClientRect().left;
    if (
      knob.getAttribute('active') === 'true' &&
      x < pole.offsetWidth - knob.offsetWidth &&
      x > 0
    ) {
      container.setAttribute(
        'data-value',
        parseInt(x.mapTo(1, pole.offsetWidth - knob.offsetWidth - 1, 0, 100)),
      );
      knob.style.transform = 'translateX(' + x + 'px)';
    }
  };

  container.addEventListener(
    'mousedown',
    e => {
      startDrag(e.clientX, pole, knob);
      e.preventDefault();
    },
    false,
  );

  container.addEventListener('touchstart', e => {
    startDrag(e.touches[0].clientX, pole, knob);
  });

  document.addEventListener(
    'mousemove',
    e => {
      moveKnob(e.clientX, pole, knob);
      e.preventDefault();
    },
    false,
  );

  document.addEventListener('touchmove', e => {
    moveKnob(e.touches[0].clientX, pole, knob);
  });

  document.addEventListener(
    'mouseup',
    e => {
      stopDrag(knob);
      e.preventDefault();
    },
    false,
  );

  document.addEventListener('touchend', e => {
    stopDrag(knob);
  });

  container.appendChild(pole);
  container.appendChild(knob);
  return container;
}

function stopGeolocation() {
  osMap
    .getOverlays()
    .getArray()
    .slice(0)
    .forEach(overlay => {
      if (overlay) osMap.removeOverlay(osMap.getOverlayById(overlay.id));
    });
  navigator.geolocation.clearWatch(geoId);
  users = {};
  menu.infoUsers.setAttribute('content', Object.keys(users).length);
  menu.infoLocateBtn.innerHTML = 'Start';
  menu.infoLocating.innerHTML = 'Locating : ';
  menu.infoLocating.classList.remove('locating');
  menu.infoLocating.classList.remove('located');
  Object.keys(synths).forEach(id => {
    synths[id].stop();
    delete synths[id];
  });
  socket.emit('unregister', socket.id);
  socket.close();
  menu.infoLocateBtn.onclick = startGeolocation;
}

function startGeolocation() {
  if (navigator.geolocation) {
    if (socket) {
      menu.infoLocateBtn.innerHTML = 'Stop';
      menu.infoLocating.classList.add('locating');
      geoId = navigator.geolocation.watchPosition(success, error, {
        enableHighAccuracy: true,
      });
      menu.infoLocateBtn.onclick = stopGeolocation;
    } else {
      alert('Connection failed!');
    }
  } else {
    alert('No geolocation');
  }
}

function createMenu() {
  const menu = {
    infoContainer: addNewElement('', 'DIV', 'infoContainer'),
    infoContent: addNewElement('', 'DIV', 'infoContent'),
    infoUsers: addNewElement('Users : ', 'DIV', 'infoUsers'),
    infoLocating: addNewElement('Locating : ', 'DIV', 'infoLocating'),
    infoHide: addNewElement('>', 'DIV', 'infoHide'),
    infoLocateBtn: addNewElement('Start', 'DIV', 'infoLocateBtn'),
    infoVolumeSlider: addSlider('volume'),
  };

  menu.infoUsers.setAttribute('content', 0);

  menu.infoHide.onclick = () => {
    if (!menu.infoContainer.classList.contains('hide')) {
      menu.infoHide.innerHTML = '<';
      menu.infoHide.classList.add('showHide');
      menu.infoContainer.classList.add('hide');
    } else {
      menu.infoHide.innerHTML = '>';
      menu.infoHide.classList.remove('showHide');
      menu.infoContainer.classList.remove('hide');
    }
  };

  menu.infoLocateBtn.onclick = startGeolocation;

  menu.infoContent.appendChild(menu.infoLocateBtn);
  menu.infoContent.appendChild(menu.infoUsers);
  menu.infoContent.appendChild(menu.infoLocating);
  menu.infoContainer.appendChild(menu.infoHide);
  menu.infoContainer.appendChild(menu.infoContent);
  menu.infoContainer.appendChild(menu.infoVolumeSlider);
  document.querySelector('body').appendChild(menu.infoContainer);
  return menu;
}

socket.on('data', data => {
  const overlay = osMap.getOverlayById(data.id);
  if (overlay) {
    const p = transform(
      [data.longitude, data.latitude],
      'EPSG:4326',
      'EPSG:3857',
    );
    overlay.setPosition(p);
		//osMap.getView().setCenter(p);
  }

  users[data.id] = data;
  menu.infoUsers.setAttribute('content', Object.keys(users).length);
});

socket.on('signal', data => {
  Object.keys(synths).forEach(id => {
    synths[id].freq(220 * data);
  });
});

socket.on('user_left', id => {
  osMap.removeOverlay(osMap.getOverlayById(id));
  delete users[id];
  menu.infoUsers.setAttribute('content', Object.keys(users).length);
});

socket.on('user_joined', newUsers => {
  Object.keys(newUsers).forEach(id => {
    if (!(id in users)) {
      const user = newUsers[id];
      users[id] = user;
      const e = addNewElement('', 'DIV', 'marker');
      e.setAttribute('id', user.id);
      const p = transform(
        [user.longitude, user.latitude],
        'EPSG:4326',
        'EPSG:3857',
      );
      const m = new Overlay({
        position: p,
        positioning: 'center-center',
        element: e,
        stopEvent: false,
        id: id,
      });
      osMap.addOverlay(m);
      if (id == socket.id) {
        osMap.getView().setCenter(p);
        osMap.getView().setZoom(12);
      }
      synths[id] = new p5.Oscillator();
      synths[id].start();
    }
  });
  menu.infoUsers.setAttribute('content', Object.keys(users).length);
});

localStorage.openpages = Date.now();
var onLocalStorageEvent = function(e) {
  if (e.key == 'openpages') {
    localStorage.page_available = Date.now();
  }
  if (e.key == 'page_available') {
    alert('One more page already open');
  }
};

window.addEventListener('storage', onLocalStorageEvent, false);
